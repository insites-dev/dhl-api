# # SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipper_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsShipperDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsShipperDetails.md) |  |
**receiver_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsReceiverDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsReceiverDetails.md) |  |
**buyer_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsBuyerDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsBuyerDetails.md) |  | [optional]
**importer_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsImporterDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsImporterDetails.md) |  | [optional]
**exporter_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsExporterDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsExporterDetails.md) |  | [optional]
**seller_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsSellerDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsSellerDetails.md) |  | [optional]
**payer_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsPayerDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsPayerDetails.md) |  | [optional]
**ultimate_consignee_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsUltimateConsigneeDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsUltimateConsigneeDetails.md) |  | [optional]
**broker_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsBrokerDetails**](SupermodelIoLogisticsExpressCreateShipmentRequestCustomerDetailsBrokerDetails.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
