# # ShipmentLimitationsByPiece

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**piece_size_limit1** | [**\OpenAPI\Client\Model\ValueUnit**](ValueUnit.md) |  | [optional]
**piece_size_limit2** | [**\OpenAPI\Client\Model\ValueUnit**](ValueUnit.md) |  | [optional]
**piece_size_limit3** | [**\OpenAPI\Client\Model\ValueUnit**](ValueUnit.md) |  | [optional]
**max_piece_weight** | [**\OpenAPI\Client\Model\ValueUnit**](ValueUnit.md) |  | [optional]
**html** | **string** | Obsolete. This is planned to be removed in future releases. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
