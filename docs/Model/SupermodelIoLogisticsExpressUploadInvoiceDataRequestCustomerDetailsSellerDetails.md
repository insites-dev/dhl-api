# # SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsSellerDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postal_address** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressAddress**](SupermodelIoLogisticsExpressAddress.md) |  |
**contact_information** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressContact**](SupermodelIoLogisticsExpressContact.md) |  |
**type_code** | **string** | Please enter the business party type of the buyer | [optional]
**registration_numbers** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRegistrationNumbers[]**](SupermodelIoLogisticsExpressRegistrationNumbers.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
