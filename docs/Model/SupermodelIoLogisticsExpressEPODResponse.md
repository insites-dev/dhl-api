# # SupermodelIoLogisticsExpressEPODResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documents** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressEPODResponseDocumentsInner[]**](SupermodelIoLogisticsExpressEPODResponseDocumentsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
