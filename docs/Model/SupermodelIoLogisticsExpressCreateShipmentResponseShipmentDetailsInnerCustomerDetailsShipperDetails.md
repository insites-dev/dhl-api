# # SupermodelIoLogisticsExpressCreateShipmentResponseShipmentDetailsInnerCustomerDetailsShipperDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postal_address** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressAddressCreateShipmentResponse**](SupermodelIoLogisticsExpressAddressCreateShipmentResponse.md) |  | [optional]
**contact_information** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressContactCreateShipmentResponse**](SupermodelIoLogisticsExpressContactCreateShipmentResponse.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
