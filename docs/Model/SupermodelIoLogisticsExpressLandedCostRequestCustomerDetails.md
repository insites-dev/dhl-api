# # SupermodelIoLogisticsExpressLandedCostRequestCustomerDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipper_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressAddressRatesRequest**](SupermodelIoLogisticsExpressAddressRatesRequest.md) |  |
**receiver_details** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressAddressRatesRequest**](SupermodelIoLogisticsExpressAddressRatesRequest.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
