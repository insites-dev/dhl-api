# # SupermodelIoLogisticsExpressDeliveryOptionsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_options** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressDeliveryOptionsResponseDeliveryOptionsInner[]**](SupermodelIoLogisticsExpressDeliveryOptionsResponseDeliveryOptionsInner.md) | Contains available deliveryOptions for the shipment |
**warnings** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
