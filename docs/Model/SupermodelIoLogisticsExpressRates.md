# # SupermodelIoLogisticsExpressRates

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\OpenAPI\Client\Model\ProductsInner1[]**](ProductsInner1.md) |  |
**exchange_rates** | [**\OpenAPI\Client\Model\ExchangeRatesInner[]**](ExchangeRatesInner.md) |  | [optional]
**warnings** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
