# # ShipmentLimitations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_number_of_pieces** | [**\OpenAPI\Client\Model\ValueUnit**](ValueUnit.md) |  | [optional]
**max_shipment_weight** | [**\OpenAPI\Client\Model\ValueUnit**](ValueUnit.md) |  | [optional]
**html** | **string** | Obsolete. This is planned to be removed in future releases. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
