# # Holidays

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**open** | [**\OpenAPI\Client\Model\OpenDatesTime[]**](OpenDatesTime.md) | Array of objects: {date, from, to}, where date is date and from and to is time | [optional]
**closed** | [**\OpenAPI\Client\Model\ClosedDates[]**](ClosedDates.md) | Array of dates:{from, to} when is closed. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
