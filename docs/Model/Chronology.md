# # Chronology

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**zone** | [**\OpenAPI\Client\Model\DateTimeZone**](DateTimeZone.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
