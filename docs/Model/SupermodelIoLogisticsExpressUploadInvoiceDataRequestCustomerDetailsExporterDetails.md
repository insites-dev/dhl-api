# # SupermodelIoLogisticsExpressUploadInvoiceDataRequestCustomerDetailsExporterDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postal_address** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressAddress**](SupermodelIoLogisticsExpressAddress.md) |  |
**contact_information** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressContact**](SupermodelIoLogisticsExpressContact.md) |  |
**registration_numbers** | [**\OpenAPI\Client\Model\SupermodelIoLogisticsExpressRegistrationNumbers[]**](SupermodelIoLogisticsExpressRegistrationNumbers.md) |  | [optional]
**type_code** | **string** | Please enter the business party type of the exporter | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
